# IMDB text classification CNN using Tensorflow

Large Movie Review Dataset. This is a dataset for binary sentiment classification, containing a set of 25,000 highly polar movie reviews from IMDB.

> This example demonstrates the use of Convolution1D for text classification. Gets to 0.89 test accuracy after 2 epochs. 90s/epoch on Intel i5 2.4Ghz CPU. 17s/epoch on NVIDEA GTX 1050 GPU.

> An example input to interact with the model can be found in `input.json`
